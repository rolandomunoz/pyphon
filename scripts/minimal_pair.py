import re

class Word:
    '''Esta clase representa una palabra con sus respectivos fonemas'''
    set_characters = list()
    
    def __init__ (self):
        self.oWord = list()
        
    def set_characters_as_one_unit(self, set_characters, clear = False):
        if clear:
            self.set_characters = list()            
        self.set_characters = set_characters
    
    def convert(self, iWord, clear = True):
        if clear:
            self.oWord = list()
           
        self.iWord = iWord
        
        while iWord != '':
            for char in self.set_characters:
                if iWord.startswith(char):
                    iWord = iWord.split(char,1)[1]
                    self.oWord.append(char)
                    break

            else:
                if len(iWord) > 0:
                    self.oWord.append(iWord[0])
                    iWord = iWord[1:]
                    
        return tuple(self.oWord)

class MinimalPair(Word):

    def __init__(self):
        self.base_word = None
        self.variable_word = None
        self.stake = None
        self.dict_minimalPairs = dict()
        self.dict_key = None 

    def is_minimalPair(self, base_word, variable_word):
        '''Determina si dos palabras son pares mínimos. Acepta dos variables de tipo str. Retorna True si son pares mínimos, en caso contrario el valor se vuelve False'''
        
        #Convertir a tuplas las variables base_word y variable_word      
        self.base_word = self.convert(base_word)
        self.variable_word = self.convert(variable_word)
               
        #Si las palabras analizadar no poseen el mismo número de fonemas, entonces detener el script. 
        if len(self.base_word) != len(self.variable_word):
            return
      
        #Recorrer cada fonema de la palabra y detectar los puntos en donde se diferencian. En caso diferenciarse en solo un fonema, este será considerado como par mínimo, atribuyendo un valor True a este método y almacenando el punto en donde se encuentra esta diferencia.
        phoneDifference = 0 
        for phoneme in range(len(self.base_word)):
            if self.base_word[phoneme] != self.variable_word[phoneme]:
                phoneDifference+=1
                self.stake = phoneme
        if phoneDifference == 1:
            self.key = ''.join(self.base_word[0:self.stake])+'_'+''.join(self.base_word[self.stake+1:])
            return True
        else:
            return False
            
    def compare(self, base_word, lstWord, clear = True):
        '''Compara una palabra base dentro de una lista de palabras para determinar el conjunto de todas las palabras en la lista que forman un par mínimo junto con la palabra base'''
        
        if clear:
            self.clear()
                    
        for variable_word in set(lstWord):
            # Si las palabras a comparse no forman un par mínimo, entonces saltar a las siguientes palabras comparables.
            if not self.is_minimalPair(base_word, variable_word):
                continue
            
            # Si un patrón de par mínimo es encontrado, revisar si este ya ha sido registrado en el diccionario. En caso haber sido registrado anteriormente, revisar si las palabras que se comparan aportan nuevos pares mínimos al registro. De ser el caso, agregarlas. En caso no haber patrón el patrón del par mínimo, agregar las palabras que se comparan al registro.
            
            if not self.key in self.dict_minimalPairs:
                self.dict_minimalPairs[self.key] = [base_word, variable_word]
                
            elif self.key in self.dict_minimalPairs:
                if not base_word in self.dict_minimalPairs[self.key]:
                    self.dict_minimalPairs[self.key] = self.dict_minimalPairs.get(self.key)+[base_word]
                                    
                if not variable_word in self.dict_minimalPairs[self.key]:
                    self.dict_minimalPairs[self.key] = self.dict_minimalPairs.get(self.key)+[variable_word]
                    
    def compare_all(self, lstWord):
        self.dict_minimalPairs = dict()
        for base_word in lstWord:
            self.compare(base_word, lstWord, clear = False)
        
    def clear(self):
        self.dict_minimalPairs = dict()

    def show(self):
        print(self.dict_minimalPairs)
                
class Corpus(MinimalPair, Word):
    def __init__(self, fileName = 'wordList.txt'):
        self.fileName = open(fileName)
        self.dict_words = dict()
        
    def connect(self, fileName):
        self.fileName = open(fileName, 'r')
        
    def close(self):
        self.fileName.close()
        
    def save(self, file = 'report.txt'):
        file = open('workfile.txt', 'w')
        for key, value in list(self.dict_minimalPairs.items()):
            file.write(key+'\t'+'\t'.join(value)+'\n')
        file.close()

    def show(self):
        for key, value in list(self.dict_minimalPairs.items()):
            print(key+'\t'+'\t'.join(value))

    def read(self, clear = True):
        if clear:
            self.dict_words = dict()
            
        word_pattern = re.compile('([A-Za-z]+)')
        for line in self.fileName:
            line = line.lower()
            lst_word = word_pattern.findall(line)
            for word in lst_word:
                self.dict_words[word] = self.dict_words.get(word, 0)  + 1
                           
def main():
    # words = ['peso', 'perro', 'paso', 'piso', 'a', 'karo', 'karro']
    # test = MinimalPair()
    # test.set_characters_as_one_unit(['sh', 'ch', 'rr'])
    # test.compare_all(words)
    # test.show()
    
    test = Corpus('./text/wordList.txt')
    test.read(clear = False)
    test.set_characters_as_one_unit(['sh', 'ch', 'rr'])
    test.compare_all(list(test.dict_words.keys()))
    test.show()
    test.save()
    
if __name__ == '__main__':
    main()
