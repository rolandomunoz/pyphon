# PyPhon

A linguistic tool for doing phonology. Find minimal pairs and describe segment distribution in a annotated corpus.